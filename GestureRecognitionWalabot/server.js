const net = require("net");
const ws = require("ws");

let clients = [];

function addSocket(list, socket) {
  for (let i = 0; i < list.length; i++) {
    if (socket == list[i]) {
      return;
    }
  }
  list.push(socket);
}

function removeClient(list, socket) {
  const nn = [];
  for (let i = 0; i < list.length; i++) {
    if (list[i] == socket) {
      nn.push(i);
    }
  }
  for (let i = 0; i < nn.length; i++) {
    list.splice(i, 1);
  }
}

function sendMessageToClients(msg) {
  for (let i = 0; i < clients.length; i++) {
    clients[i].send(msg);
  }
}

const webserver = new ws.WebSocketServer({ port: 3000 });
webserver.on("connection", (socket) => {
  addSocket(clients, socket)
  socket.on("message", (msg) => {
    console.log(`Recieved a message from client: ${msg.toString()}`)
  });
  socket.on("error", (err) => {
    console.log("client error: ", err);
    removeClient(clients, socket);
  });
  socket.on("close", () => {
    console.log("client left");
    removeClient(clients, socket);
  });
})

const webServer = net.createServer((socket) => {
  // console.log("server");
  // socket.write("Test");
  // socket.pipe(socket);
  socket.on("data", (data) => {
    data = data.toString();
    console.log(`Recieved from client: ${data}`)
    sendMessageToClients(data);
  });
});

webServer.listen(3002, () => {
  console.log("Socket connected...");
});

