#include "Actions.h"


std::ofstream file("log.txt");
#ifndef _SAMPLE_CODE_
#pragma comment(lib, "Ws2_32.lib")
int main()
{
	WSAStartup(MAKEWORD(2, 0), &WSAData);
	server = socket(AF_INET, SOCK_STREAM, 0);

	addr.sin_addr.s_addr = inet_addr("127.0.0.1"); // replace the ip with your futur server ip address. If server AND client are running on the same computer, you can use the local ip 127.0.0.1
	addr.sin_family = AF_INET;
	addr.sin_port = htons(3002);

	while (connect(server, (SOCKADDR*)&addr, sizeof(addr)) == SOCKET_ERROR)
	{
		std::cout << "Connection failed!" << std::endl;
		//return 0;
	}
	std::cout << "Connected to server!" << std::endl;
	FlipPages_SampleCode();
	//called with no parameters
	auto lam = []() { 
		std::cout << "at exit"; 
		file.close(); 
		//  Stop and Disconnect.
		//  ======================
		res = Walabot_Stop();
		CHECK_WALABOT_RESULT(res, "Walabot_Stop");
		res = Walabot_Disconnect();
		CHECK_WALABOT_RESULT(res, "Walabot_Disconnect");
		closesocket(server);
		WSACleanup();
		std::cout << "Socket closed." << std::endl << std::endl;
	};

	atexit(lam);
	
}
#endif