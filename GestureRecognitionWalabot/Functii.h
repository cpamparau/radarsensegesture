#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include "WalabotAPI.h"
#include <stdio.h>
#include <iostream>
#include <conio.h>
#include <string>
#define WINVER 0x0500
#include <winsock2.h>
#include <windows.h>
#include <fstream>
#include <iostream>
#include <cstdlib>

#ifdef __LINUX__
#define CONFIG_FILE_PATH "/etc/walabotsdk.conf"
#else
#define CONFIG_FILE_PATH "C:\\Program Files\\Walabot\\WalabotSDK\\bin\\.config"
#endif

#define CHECK_WALABOT_RESULT(result, func_name)                 \
{                                                               \
    if (result != WALABOT_SUCCESS)                              \
    {                                                           \
        unsigned int extended = Walabot_GetExtendedError();     \
        const char* errorStr = Walabot_GetErrorString();        \
        std::cout << std::endl << "Error at " __FILE__ << ":"   \
                  << std::dec << __LINE__ << " - "              \
                  << func_name << " result is 0x" << std::hex   \
                  << result << std::endl;                       \
                                                                \
        std::cout << "Error string: " << errorStr << std::endl; \
                                                                \
        std::cout << "Extended error: 0x" << std::hex           \
                  << extended << std::endl << std::endl;        \
                                                                \
        std::cout << "Press enter to continue ...";             \
        std::string dummy;                                      \
        std::getline(std::cin, dummy);                          \
        return;                                                 \
    }                                                           \
}

// Global Variables
int gFlag = 0;                      // 0 when the array of ypos samples needs to be completely reset with new samples
const double gDeltaY = 0.0;         // The difference by which 2 successive ypos samples, are checked for increasing/decreasing tendency 
const int gSampleSizeWindow = 10;    // Number of ypos samples to be taken at each loop 
int gCleanSampleSize = 0;           // Number of yPos samples received after a single loop (not every trigger action is guaranteed to return a target)
int gRestTime = 1000;                // Amount of milliseconds to rest after identifying a gesture and turning a page (gives time to move the hand out 
                                    //  of the arena to avoid an unintentional reading)
// Returns true if a there is at least one positive y position in YposSorted
//client c;
//websocketpp::connection_hdl m_hdl;
std::string uri = "ws:\/\/192.168.10.66:3000";
WSADATA WSAData;
SOCKET server;
SOCKADDR_IN addr;
WALABOT_RESULT res;

typedef struct date {
    double y;
    double z;
} date;
bool GotPositive(date YposSorted[])
{
    for (int k = 0; k < gCleanSampleSize; k++)
    {
        if (YposSorted[k].y > 0)
        {
            return true;
        }
    }
    return false;
}
// Returns true if a there is at least one negative  y position in YposSorted
bool GotNegative(date YposSorted[])
{
    for (int k = 0; k < gCleanSampleSize; k++)
    {
        if (YposSorted[k].y < 0)
        {
            return true;
        }
    }
    return false;
}


// Given the array YposArray - moves the actual target values to the front of the array
date* SortYposArray(date YposArray[])
{
    date YposSorted[gSampleSizeWindow];
    for (int k = 0; k < gSampleSizeWindow; k++)
    {
        if (YposArray[k].y < 617)
        {
            YposSorted[gCleanSampleSize] = YposArray[k];
            gCleanSampleSize++;
        }
    }
    return YposSorted;
}
// Returns a score by checking the direction of the y coordinates in the array YposSorted
int GetScore(date YposSorted[])
{
    int score = 0;
    for (int k = 1; k < gCleanSampleSize; k++)
    {
        if ((YposSorted[k].y - YposSorted[k - 1].y) < gDeltaY)
        {
            score++;
        }
        else if ((YposSorted[k].y - YposSorted[k - 1].y) > gDeltaY)
        {
            score--;
        }
    }
    return score;
}

double GetSum(date x[])
{
    double s = 0;
    for (int k = 0; k < gCleanSampleSize; k++)
        s += x[k].y;
    return s;
}

double GetAverage(date x[])
{
    double s = x[0].z;
    for (int k = 0; k < gCleanSampleSize; k++)
        if (s < x[k].z)
            s = x[k].z;
    return s;
}



// Get y of closest target projected on the Y-Z plane
date GetYposOfClosestTarget(SensorTarget* targets, int numTargets)
{
	// Set initial parameter values
	int ClosestTargetIndex = 0;
	double SizeOfClosestTargetYZ = sqrt(pow(targets[0].yPosCm, 2) + pow(targets[0].zPosCm, 2));
    date aux;
	for (int k = 1; k < numTargets; k++)
	{
		double SizeOfTargetYZ = sqrt(pow(targets[k].zPosCm, 2) + pow(targets[k].yPosCm, 2));
		if (SizeOfTargetYZ < SizeOfClosestTargetYZ)
		{
			ClosestTargetIndex = k;
			SizeOfClosestTargetYZ = SizeOfTargetYZ;
		}
	}
    aux.y = targets[ClosestTargetIndex].yPosCm;
    aux.z = targets[ClosestTargetIndex].zPosCm;
	return aux;
}
// Get y coordinate of sensed target
date GetYpos(SensorTarget* targets, int numTargets)
{
	if (numTargets > 1)
	{
		return GetYposOfClosestTarget(targets, numTargets);
	}
	else if (numTargets == 1)
	{
        date aux;
        aux.z = targets->zPosCm;
        aux.y = targets->yPosCm;
		return aux;
	}
	else
	{
        date aux;
        aux.y = 617;
        aux.z = -2;
		return aux; // A value that is out of range at any case- marks that no actual target was detected
	}
}

