#pragma once
#include "Functii.h"



// Flips page
void PageFlip(date  YposArray[])
{
	date* YsortedPointer;
	YsortedPointer = SortYposArray(YposArray);
	//std::cout << "YsortedPointer: " << YsortedPointer << std::endl;
	date YposSorted[gSampleSizeWindow];
	
	for (int k = 0; k < gCleanSampleSize; k++)
	{
		YposSorted[k].y = YsortedPointer[k].y;
		YposSorted[k].z = YsortedPointer[k].z;
	}
	if (GotNegative(YposSorted) == true && GotPositive(YposSorted) == true)
	{
		double Score = GetScore(YposSorted);
		double Sum = GetSum(YposSorted);
		double average = GetAverage(YposSorted);
		//
		int CurrentCleanSampleSize = gCleanSampleSize;
		//for (int i = 0; i < CurrentCleanSampleSize; i++)
		//	std::cout << "amplitude=" << YposSorted[i].z << "\t"; std::cout << "\n";
		gCleanSampleSize = 0; // RESET VALUE
		if ( Sum > 0 && Score > 0) // Score > 0 && Score > (CurrentCleanSampleSize / 2.5)
		{
			INPUT ip;
			// Set up a generic keyboard event.
			ip.type = INPUT_KEYBOARD;
			ip.ki.wScan = 0; // hardware scan code for key
			ip.ki.time = 0;
			ip.ki.dwExtraInfo = 0;
			// Press the "PgDn" key
			ip.ki.wVk = 0x22; // virtual-key code for the "PgDn" key
			ip.ki.dwFlags = 0; // 0 for key press
			SendInput(1, &ip, sizeof(INPUT));
			// Release the "PgDn" key
			ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
			SendInput(1, &ip, sizeof(INPUT));
			gFlag = 0;
			char buffer[] = "dreapta";
			//std::cout << buffer << std::endl;
			int res = 0;
			//websocketpp::lib::error_code ec;
			//c.send(m_hdl, std::string(buffer), websocketpp::frame::opcode::text, ec);
			//if (ec) {
			//	std::cout << "Echo failed because: " << ec.message() << std::endl;
			//}
			res = send(server, buffer, sizeof(buffer), 0);
			//std::cout << "[";
			//for(int k=0;k<copy;k++)
				//std::cout << YposSorted[k] << "  ";
			std::cout << "media= " << average << ", suma= " << Sum <<" Am trimis " << res << "  octeti!\n";
			//std::cout << "Message sent!" << std::endl;
			Sleep(gRestTime);
		}
		else if ( Sum < 0 && Score <0) // || Score < 0 && abs(Score) >(CurrentCleanSampleSize / 2.5)
		{
			INPUT ip;
			// Set up a generic keyboard event.
			ip.type = INPUT_KEYBOARD;
			ip.ki.wScan = 0; // hardware scan code for key
			ip.ki.time = 0;
			ip.ki.dwExtraInfo = 0;
			// Press the "PgUp" key
			ip.ki.wVk = 0x21; // virtual-key code for the "PgUp" key
			ip.ki.dwFlags = 0; // 0 for key press
			SendInput(1, &ip, sizeof(INPUT));
			// Release the "PgUp" key
			ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
			SendInput(1, &ip, sizeof(INPUT));
			gFlag = 0;
			char buffer[] = "stanga";
			int res = send(server, buffer, sizeof(buffer), 0);
			//std::cout << "[";
			//for (int k = 0; k < copy; k++)
				//std::cout << YposSorted[k] << "  ";
			std::cout << "media= " << average << ", suma= " << Sum << " Am trimis " << res << "  octeti!\n";
			//std::cout << buffer << std::endl;
			//websocketpp::lib::error_code ec;
			//c.send(m_hdl, std::string(buffer), websocketpp::frame::opcode::text, ec);
			//if (ec) {
			//	std::cout << "Echo failed because: " << ec.message() << std::endl;
			//}
			Sleep(gRestTime);
		}
		else
		{
			return;
		}
	}
	else
	{
		gCleanSampleSize = 0; // RESET VALUE
		return;
	}
}


void FlipPages_SampleCode()
{
	// --------------------
	// Variable definitions
	// --------------------

	// Walabot_GetStatus - output parameters
	APP_STATUS appStatus;
	double calibrationProcess; // Percentage of calibration completed, if status is STATUS_CALIBRATING
	SensorTarget* targets;
	int numTargets;
	date YposArray[gSampleSizeWindow];
	// ------------------------
	// Initialize configuration
	// ------------------------ 
	double rArenaMin = 10.0;
	double rArenaMax = 100.0;
	double rArenaRes = 3.0;
	double thetaArenaMin = -20.0;
	double thetaArenaMax = 20.0;
	double thetaArenaRes = 2.0;
	double phiArenaMin = -45.0;
	double phiArenaMax = 45.0;
	double phiArenaRes = 2.0;
	double threshold = 35.0;
	// Configure Walabot database install location (for windows)
	//char* s = const_cast<char*>("C:/Program Files/Walabot/WalabotSDK");
	//res = Walabot_SetSettingsFolder(s);
	//CHECK_WALABOT_RESULT(res, "Walabot_SetSettingsFolder");
	res = Walabot_Initialize(CONFIG_FILE_PATH);
	CHECK_WALABOT_RESULT(res, "Walabot_Initialize");
	//  Connect : Establish communication with Walabot.
	//  ==================================================
	res = Walabot_ConnectAny();
	CHECK_WALABOT_RESULT(res, "Walabot_ConnectAny");
	//  Configure : Set scan profile and arena
	//  =========================================
	//   Set Profile - SENSOR Profile
	//   Walabot recording mode is configured with the following attributes:
	//   -> Distance scanning through air; 
	//   -> High-resolution images, but slower capture rate;
	res = Walabot_SetProfile(PROF_SENSOR);
	CHECK_WALABOT_RESULT(res, "Walabot_SetProfile");
	// Set arena size and resolution with Polar coordinates:
	res = Walabot_SetArenaR(rArenaMin, rArenaMax, rArenaRes);
	CHECK_WALABOT_RESULT(res, "Walabot_SetArenaR");
	res = Walabot_SetArenaTheta(thetaArenaMin, thetaArenaMax, thetaArenaRes);
	CHECK_WALABOT_RESULT(res, "Walabot_SetArenaTheta");
	res = Walabot_SetArenaPhi(phiArenaMin, phiArenaMax, phiArenaRes);
	CHECK_WALABOT_RESULT(res, "Walabot_SetArenaPhi");
	// Set threshold
	res = Walabot_SetThreshold(threshold);
	CHECK_WALABOT_RESULT(res, "Walabot_SetThreshold");
	// Set Walabot filtering type
	res = Walabot_SetDynamicImageFilter(FILTER_TYPE_MTI);
	CHECK_WALABOT_RESULT(res, "Walabot_SetDynamicImageFilter");
	// Start: Start the system in preparation for scanning.
	//  =======================================================
	res = Walabot_Start();
	CHECK_WALABOT_RESULT(res, "Walabot_Start");
	// calibrates scanning to ignore or reduce the signals
	res = Walabot_StartCalibration();
	CHECK_WALABOT_RESULT(res, "Walabot_StartCalibration");
	// calibrates scanning to ignore or reduce the signals
	res = Walabot_GetStatus(&appStatus, &calibrationProcess);
	CHECK_WALABOT_RESULT(res, "Walabot_GetStatus");
	// Display message to the user:
	std::cout << "The app is up and running- to use the app, bring your file into focus" << '\n';
	// Loop 
	bool recording = true;
	while (recording)
	{
		if (gFlag == 0)
		{
			for (int k = 0; k < gSampleSizeWindow; k++)
			{
				//  Trigger: Scan(sense) according to profile and record signals to be 
				//  available for processing and retrieval.
				//  ====================================================================
				res = Walabot_Trigger();

				CHECK_WALABOT_RESULT(res, "Walabot_Trigger");
				//   Get action : retrieve the last completed triggered recording 

				//  ================================================================
				res = Walabot_GetSensorTargets(&targets, &numTargets);
				//std::cout << "Targets: " << numTargets << std::endl;
				CHECK_WALABOT_RESULT(res, "Walabot_GetSensorTargets");
				YposArray[k] = GetYpos(targets, numTargets);
			}
			gFlag = 1;
			PageFlip(YposArray);
		}
		if (gFlag == 1)
		{
			for (int k = 1; k < gSampleSizeWindow; k++)
			{
				YposArray[k - 1] = YposArray[k];
			}
			res = Walabot_Trigger();
			CHECK_WALABOT_RESULT(res, "Walabot_Trigger");
			res = Walabot_GetSensorTargets(&targets, &numTargets);
			//if (numTargets)
				//std::cout << "Targets: " << numTargets << std::endl;
			CHECK_WALABOT_RESULT(res, "Walabot_GetSensorTargets");
			YposArray[gSampleSizeWindow - 1] = GetYpos(targets, numTargets);
			PageFlip(YposArray);
		}
	}
	//  Stop and Disconnect.
	//  ======================
	res = Walabot_Stop();
	CHECK_WALABOT_RESULT(res, "Walabot_Stop");
	res = Walabot_Disconnect();
	CHECK_WALABOT_RESULT(res, "Walabot_Disconnect");
}
